reddit
==============================

Reddit Fun

Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org

Anaconda Installation and Usage
-------------------------------

This uses [Anaconda](https://www.anaconda.com/) as the package manager.
You can install it by downloading and running a package from [here](https://www.anaconda.com/distribution/) which is appropriate for your system.
This installs the `conda` command which is used for all virtual environment operations.
You can read the documentation [here](https://docs.anaconda.com/anaconda/).
You can search for dependencies [here](https://anaconda.org/) (you do not need to sign up).

Anaconda uses named virtual environments.
The first thing you should do is pick a name for your virtual environment - the name of the project would be great.
Then create and activate it:

```
conda create --name ENVIRONMENT
conda activate ENVIRONMENT
```

When using this template you should make sure you update the name of the virtual environment in the `environment.yml` file:
```
name: reddit
```
If you choose to change this then you should change the associated project name in the Makefile:
```
PROJECT_NAME = reddit
```

Then you can create the virtual environment using the `make` command:
```
make requirements
```

To install any new packages you need to activate the environment:
```
conda activate ENVIRONMENT
```
Then you can install the package with the following command:
```
conda install DEPENDENCY
```
Sometimes the install command is more involved, if it cannot find your package search for it on [anaconda cloud](https://anaconda.org/).
The install command will be on the package page.

When you add a dependency make sure you record that in the `environment.yml` or other people will not be able to recreate your environment.
You can do this with `make save`.
Make sure you have activated your project virtual environment before running this!

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
<p><small>Refined to use Anaconda as the package manager</small></p>
